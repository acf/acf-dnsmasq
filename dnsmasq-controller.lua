local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.config(self)
	return self.handle_form(self, self.model.getconfig, self.model.setconfig, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.getconfigfile, self.model.setconfigfile, self.clientdata, "Save", "Edit Config File", "Configuration File Set")
end

function mymodule.viewleases(self)
	return self.model.getleases()
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
